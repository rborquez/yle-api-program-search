﻿using UnityEngine;
using System.Collections;
using LitJson;
using UnityEngine.UI;

/*  This program uses the LitJson dll to parse the data brought back by Unity's WWW method.
	Litjson requires the data as a string format, so we need the jsonString variable to store
	the values brought back from the WWW request as string to make use of its methods. 
	The url var will store the URL link to the API and we'll then append the page number 
	integer variable to change the offset in the request to get different results every time we send a new request.
	The results variable is necessary to keep count of how many results we are showing at the moment,
	while the totalResults var is usded to calculate how many pages we should display.
	The gameButton GameObject will be instantiated a set number of times and will become the list of programs.
	The scrollBar var will be used to keep track of where the user is at, and when it reaches the 
	bottom of the screen, a new web request will be sent to show 10 more results, and increment the pageNumber.
	The boolean var isRunning prevents coroutines from being called multiple times.
*/
public class SearchPrograms : MonoBehaviour {

	//	variable declaration begins
	private string jsonString, url, urlTemp, searchQuery;  
	private JsonData programData;
	public GameObject gameButton, resultsText, searchButton, trendingButton, errorMessage;
	public Scrollbar scrollBar;
	public InputField mainInputField;
	private int pageNumber, totalPages, resultNumber, totalResults, limit, offset, resultsLeft;
	private bool isRunning, isDonePaging;

	//	Using the start method to call our first 10 results using a search query for the most popular
	//	programs of the day as the first screen in the app.
	void Start () {

		errorMessage.SetActive (false);
		//	Number of results we want to bring from the API
		limit = 10;
		url = "https://external.api.yle.fi/v1/programs/items.json?app_id=fc1d4798&" +
			"app_key=a9efd1c515edd26ad13daecc942e6ad2&availability=ondemand&limit=" + limit + "&playcount.24h:desc" +
			"&type=TVprogram&offset=" + offset;
		pageNumber = 1;
		SearchMostPopularPrograms();
		//	Creating a listener for the scrollBar, this will help in keeping track of the scrollBar value
		//	without having to use the Update() method.
		scrollBar.onValueChanged.AddListener(delegate { CheckIfAtBottom(scrollBar);});
	}

	void Update () {

		// Used to constantly redraw the current page and the number of total pages.
		resultsText.GetComponent<Text>().text = "Page " + pageNumber + " of " + totalPages;

	}
		
	//	Standard coroutine used to wait until we finish downloading the data from the API.
	IEnumerator WaitForRequest(WWW www) {

		if (isRunning)
			Debug.Log ("Coroutine can't be launched, already running.");
		else {
			Debug.Log ("Coroutine launched.");
			isRunning = true;
			yield return www;

			if (www.error == null) {
				Debug.Log ("API request successful!: " + www.text);
			} else {
				Debug.Log ("API reques failed: " + www.error);
			}
		}
	}

	//	This function is used to keep track of the scrollbar value.
	//	When it reaches 0, a new request to the API is sent to bring the next results and the page number increments.
	public void CheckIfAtBottom (Scrollbar scrollBar){

		float scrollBarValue = scrollBar.value;
		//	isDonePaging is important to tell the function that we have reached the last page.
		if (scrollBarValue == 0 &&  isDonePaging == false) {
			offset++;
			pageNumber++;
			//	GetJsonAsString will bring the contents of the WWW request as text.
			jsonString = GetJsonAsString (urlTemp);
			//	This is the function that is in charge of drawing the buttons.
			createNewList (gameButton, jsonString);
		}
	}

	/*	This function does most of the heavy lifting. The first thing it does is grab the string var cointaining 
	 * 	all the data that we gathered from the WWW request and make use of the LitJson library to parse the information.
	 * 	This makes it possible to navigate the keys inside of the JSON file. The first thing we do is get the total number
	 * 	of results, because they will be used by another function to get the total number of pages and how many results will
	 * 	be left on the last page. 
	 * 	GetNumberOfPages() will give us the different scenarios upon which we will draw the buttons and change their text component
	 * 	to the corresponding program name contained inside the JSON object.
	 */
	public void createNewList (GameObject programButton,  string stringToParse){

		JsonData programData;
		string programName;


		programData = JsonMapper.ToObject (stringToParse);
		totalResults = (int)programData ["meta"] ["program"];
		GetNumberOfPages (totalResults);
		//	If we have at least 1 result
		if (totalResults != 0) {
			//	If the total number of pages is 1 or we are on the last page already.
			if (totalPages == 1 || pageNumber == totalPages) {
				for (int i = 0; i < resultsLeft; i++) {
					//	Finished changing pages
					isDonePaging = true;
					//	Useful for debuging, could have other implementations.
					resultNumber++;
					GameObject createdButton = (GameObject)Instantiate (programButton);
					//	Attaching the buttons to the grid we setup with a vertical layout group.
					createdButton.transform.SetParent (GameObject.Find ("ElementsGrid").transform);
					//	Extracting the title from each JSON object 
					programName = (string)programData ["data"] [i] ["title"] ["fi"];
					Debug.Log ("Result number " + resultNumber + ": " + programName);
					//	Changing the text component to the program title.
					createdButton.GetComponentInChildren<Text> ().text = programName;
				}
				//	If we are on the first (with more than 1 total pages) or middle pages.
			} else {
				for (int i = 0; i < limit; i++) {
					//	Not finished chainging pages.
					isDonePaging = false;
					resultNumber++;
					GameObject createdButton = (GameObject)Instantiate (programButton);
					createdButton.transform.SetParent (GameObject.Find ("ElementsGrid").transform);
					programName = (string)programData ["data"] [i] ["title"] ["fi"];
					Debug.Log ("Result number " + resultNumber + ": " + programName);
					createdButton.GetComponentInChildren<Text> ().text = programName;

				}
				Debug.Log ("Currently at " + pageNumber + " of " + totalPages);
			}
			//	If the search query didn't get any results back.	
		}  else {
			isDonePaging = true;
			//	Display error message.
			errorMessage.SetActive (true);
			//	Specify which searchquery failed to bring results in the text.
			errorMessage.GetComponentInChildren<Text> ().text = "No results for: " + searchQuery;
			Debug.Log ("No results for: " + searchQuery);

		}
	}

	// This function is in charge of sending the WWW requests to the server and storing the data as a string.
	public string GetJsonAsString (string url) {

		//	using WWW method to send a request to the API
		WWW apiRequest = new WWW (url);
		//	starting coroutine to wait for the server response
		StartCoroutine(WaitForRequest (apiRequest));
		//	pause needed while data is downloaded
		while(!apiRequest.isDone) {}
		string jsonString = apiRequest.text;
		Debug.Log ("My jsonString is: " + jsonString);
		//	sending back the JSON object as a string to be parsed
		return jsonString;
	}

	//	Function to get the number of pages and how many results to be displayed on the last page.
	void GetNumberOfPages (int numberOfResults){

		totalPages = numberOfResults / limit;
		resultsLeft = numberOfResults % limit;
		Debug.Log ("Total number of results: " + totalResults);
		//	Integers always round down when divided; this if statement is used to specify that getting
		//	a 0 in the algorithm means that the number of pages is in fact 1, not 0.
		if (totalPages != 0) {
			//	In case there are results left in last page, e.g 45 results = 5 pages, 5 results.
			if (resultsLeft != 0) {
				totalPages++;
				Debug.Log ("Total pages is: " + totalPages);
				Debug.Log ("Total number of results to display in last page: " + resultsLeft);
				//	In case there are no results left in last page, e.g 40 results = 4 pages 0 results.
			} else {
				resultsLeft = 10;
				Debug.Log ("Total pages is: " + totalPages);
				Debug.Log ("Total number of results to display in last page: " + resultsLeft);
			}
			//	In case the division is 0; which means a single page.
		} else {

			totalPages = 1;
			//	In case there are results left on the single page, e.g 8 results = 1 pages 8 results.
			if (resultsLeft != 0) {
				Debug.Log ("Total pages is: " + totalPages);
				Debug.Log ("Total number of results to display in last page: " + resultsLeft);
			//	In case there were no results at all from the very beginning. 
			} else if (numberOfResults == 0) {
				Debug.Log ("Total pages is: " + totalPages);
				Debug.Log ("Total number of results to display in last page: " + resultsLeft);
			//	In the very specific case of getting 10 results, resultsLeft will show 0, when in fact, there are 10.
			} else {
				resultsLeft = 10;
				Debug.Log ("Total pages is: " + totalPages);
				Debug.Log ("Total number of results to display in last page: " + resultsLeft);
			}

		}
	}

	//	Function to create a list with the most popular shows of the day.
	//	This is the default list.
	public void SearchMostPopularPrograms (){

		//	Setting everything to their original values
		errorMessage.SetActive (false);
		DeleteAllProgramButtons ();
		offset = 0;
		pageNumber = 1;
		resultNumber = 0;
		urlTemp = url;
		string jsonTemp = GetJsonAsString (urlTemp);
		createNewList (gameButton, jsonTemp);
		scrollBar.value = 1;
	}

	//	Function to search for a specific show using the input field on the UI.
	public void SearchForProgram (InputField inputField) {

		//	Setting everything to their original values
		errorMessage.SetActive (false);
		DeleteAllProgramButtons ();
		offset = 0;
		pageNumber = 1;
		resultNumber = 0;
		searchQuery = inputField.text;
		//	The 'q' is used to tell the API that we want results related to the keyword we are sending.
		urlTemp = url + "&q=" + searchQuery;
		string jsonTemp = GetJsonAsString (urlTemp);
		createNewList (gameButton, jsonTemp);
		scrollBar.value = 1;
	}

	//	Function for clearing the screen by deleting all buttons being displayed.
	void DeleteAllProgramButtons () {
		foreach (Transform child in GameObject.Find ("ElementsGrid").transform) {
			GameObject.Destroy (child.gameObject);
		}
	}
}
